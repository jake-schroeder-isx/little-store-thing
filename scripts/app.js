
const searchElement = document.querySelector('.products__input');
const productsElement = document.querySelector('.products__grid');
const bagList = document.querySelector('.bag__list');
const cartNumberElement = document.querySelector('.cart__number');
const cartCheckoutTotal = document.querySelector('.checkout__total');


const pageNumberDiv = document.querySelector(".page__number");

$( document ).ready(function() {

    cartRender({cartDataCopy: cartData});

    const bagButton = $('.bag__button');
    const bagView = $('.bagview__wrapper');
    const bagOverlay = $('.bagview__overlay');
    const closeButton = $('.closeButton');
    const prevButton = $('#prev');
    const nextButton = $('#next');
    const checkoutBtn = $('.checkout__btn');


    prevButton.click((e) => {
        console.log(e);
        getPage(e);

    });

    nextButton.click((e) => {
        console.log(e);
        getPage(e);

    });


    bagButton.click(function(e) {

        bagView.toggleClass("open");
        bagOverlay.toggleClass("open")

    });

    bagOverlay.click((e)=>{
        bagView.toggleClass("open");
        bagOverlay.toggleClass("open")
    });

    closeButton.click(() => {
        bagView.toggleClass("open");
        bagOverlay.toggleClass("open")
    });

    checkoutBtn.click((e) => {
        alert("Checkout Suscessful. You spent $" + cartTotal.toString());

        cartData = [];
        cartRender({cartDataCopy: cartData});
    });

    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 70
        }, 500);
    });

});


let products = {};
let cartData = JSON.parse(localStorage.getItem('cartData'));



let cartNumber = 0;
let cartTotal = 0.0;

let page = 1;
let limit = 10;
let search = "";




let template = `
    {{#each productsCopy}}
      <div class="card card--product" style="border: 2px solid {{color}};">
            <div class="card__wrapper">
                <div class="card__header">
                    <h4 class="card__title">{{title}}</h4>
                    <span class="tags">#{{color}}</span>
                </div>
                <div class="card__main">
        
                    <div class="products__feature">
                        <img class="products__image" src="https://via.placeholder.com/150" alt="image of product"/>
                    </div>
                    
                       <h2 class="products__price">
                        \${{price}}
                    </h2>
        
                    <button class="btn btn--add" href="#" onclick="addToCart({{ @index }})">Add to Bag</button>
                </div>
            </div>
        </div>
    {{/each}}
    
    
    `;


let bagTemplate = `
    {{#each cartDataCopy}}
     <li class="bag__item">
        <div class="card card--product" style="border: 2px solid {{color}};">
            <div class="card__wrapper">
                <div class="card__header">
                    <h4 class="card__title">{{title}}</h4>
                    <span class="tags">#{{color}}</span>
                </div>
                <div class="card__main">

                    <div class="products__feature">
                        <img class="products__image" src="https://via.placeholder.com/150" alt="image of product"/>
                    </div>
                    
                    <h2 class="products__price">
                        \${{price}}
                    </h2>

                    <button class="btn btn--add" href="#" onclick="removeProduct({{ @index }})">Remove</button>
                </div>
            </div>
        </div>

    </li>
    {{/each}}
`;


function render(context) {

    let compiled = Handlebars.compile(template);

    productsElement.innerHTML = compiled(context);

    pageNumberDiv.innerText = page.toString();

}

function cartRender(context) {

    let compiled = Handlebars.compile(bagTemplate);

    bagList.innerHTML = compiled(context);

    cartTotal = cartData.reduce(((acc, curr) => acc + parseFloat(curr.price)), 0);

    cartNumber = cartData.length;



    cartNumberElement.innerHTML = cartNumber.toString();

    cartCheckoutTotal.innerHTML = 'Total: $' + cartTotal.toString();

    localStorage.setItem('cartData', JSON.stringify(cartData));

}



function getProducts() {

    $.ajax({

        method: 'GET',
        url: 'http://5ceb36a60c871100140bf873.mockapi.io/v1/item?p=' + page + '&l=' + limit + '&search=' + search,

    }).done(function (data) {

        products = {productsCopy: data};
        render(products);

    });
}

function getPage(event) {

    if(event.target.id === 'prev') {

        if (page > 1) {
            page --;
        }

    }

    if(event.target.id === 'next') {

        if (page < 10) {
            page ++;
        } else{
            page = 1;
        }

    }

    getProducts();
}


function searchHandler(event) {


    search = event.target.value;


    getProducts();

}




getProducts();

function removeProduct(index) {

    cartTotal = cartTotal - parseFloat(cartData[index]['price']);

    cartData.splice(index, 1);

    cartNumber = cartData.length;

    let cartDataCopy = {
        cartDataCopy: cartData
    };

    cartRender(cartDataCopy);

}

function addToCart(index) {

    console.log("add to cart");

    cartData.push(products.productsCopy[index]);





    let cartDataCopy = {
     cartDataCopy: cartData
    };

    cartRender(cartDataCopy);

}



searchElement.addEventListener('input', searchHandler);

